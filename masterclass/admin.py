from django.contrib import admin
from masterclass import models

admin.site.register(models.Video)
admin.site.register(models.Quiz)

