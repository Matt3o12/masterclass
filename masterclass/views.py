from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from masterclass import models

def index(request):
    template = loader.get_template("masterclass/index.html")

    return HttpResponse(template.render({}, request))

def videos(request):
    template = loader.get_template("masterclass/video.html")
    videos = models.Video.objects.all()

    if request.method == "POST":
        id = request.POST["id"]
        model = models.Quiz.objects.filter(video__id=id).first()
        print(id,model)
        expectedAnswers = []
        for i in range(1,5):
            _get_answers(model, i, expectedAnswers)

        answers = request.POST.getlist("answer", [])
        if sorted(answers) == sorted(expectedAnswers):
            return redirect("quiz_correct")
        else:
            return redirect("quiz_wrong")

    return HttpResponse(template.render({
        "videos": videos,
    }, request))


def _get_answers(model, i, expected):
    if getattr(model, "solution{}".format(i)): 
        expected.append("answer{}".format(i))
    

def quiz(request, id):
    template = loader.get_template("masterclass/quiz.html")
    model = models.Quiz.objects.get(pk=id)

    if request.method == "POST":
        expectedAnswers = []
        for i in range(1,5):
            _get_answers(model, i, expectedAnswers)

        answers = request.POST.getlist("answer", [])
        if sorted(answers) == sorted(expectedAnswers):
            return redirect("quiz_correct")
        else:
            return redirect("quiz_wrong")

    return HttpResponse(template.render({"quiz": model}, request))

def quiz_wrong(request):
    template = loader.get_template("masterclass/validation.html")
    return HttpResponse(template.render({"correct": False}, request))

def quiz_correct(request):
    template = loader.get_template("masterclass/validation.html")
    return HttpResponse(template.render({"correct": True}, request))

