from django.db import models

class Video(models.Model):
    title = models.CharField(max_length=250)
    desc = models.TextField()
    video = models.FileField(upload_to="uploads/")

class Quiz(models.Model):
    name = models.CharField(max_length=250)
    video = models.ForeignKey("Video", on_delete="cascade")

    question1 = models.CharField(max_length=250)
    question2 = models.CharField(max_length=250)
    question3 = models.CharField(max_length=250)
    question4 = models.CharField(max_length=250)

    solution1 = models.BooleanField()
    solution2 = models.BooleanField()
    solution3 = models.BooleanField()
    solution4 = models.BooleanField()

    class Meta:
        verbose_name_plural = "Quizzes"

