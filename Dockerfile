FROM python:3.7

RUN pip install django

ADD . /app

WORKDIR /app

RUN ./manage.py migrate

CMD ./manage.py runserver 0.0.0.0:8080

